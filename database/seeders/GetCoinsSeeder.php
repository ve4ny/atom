<?php

namespace Database\Seeders;

use App\GetCoins;
use App\Models\Coins;
use Carbon\Carbon;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class GetCoinsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $coins = GetCoins::getAll(config('services.coinMarketCap.key'));
        $count = 0;
        foreach ($coins as $coin) {
            $existingRecord = Coins::where('id', $coin['id'])->first();

            if (!$existingRecord) {
                Coins::create([
                    'id' => $coin['id'],
                    'rank' => $coin['rank'],
                    'name' => $coin['name'],
                    'symbol' => $coin['symbol'],
                    'slug' => $coin['slug'],
                    'is_active' => $coin['is_active'],
                    'first_historical_data' => Carbon::createFromFormat('Y-m-d\TH:i:s.u\Z', $coin['first_historical_data'], 'UTC'),
                    'last_historical_data' => Carbon::createFromFormat('Y-m-d\TH:i:s.u\Z', $coin['last_historical_data'], 'UTC'),
                    'platform' => json_encode($coin['platform'])
                ]);

                $count++;

                if ($count >= 100) {
                    break;
                }
            }
        }
    }
}
