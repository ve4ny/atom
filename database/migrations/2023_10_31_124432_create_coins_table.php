<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('coins', function (Blueprint $table) {
            $table->integer('id');
            $table->integer('rank');
            $table->string('name', 30);
            $table->string('symbol', 30);
            $table->string('slug', 30);
            $table->boolean('is_active');
            $table->dateTime('first_historical_data');
            $table->dateTime('last_historical_data');
            $table->json('platform')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('coins');
    }
};
