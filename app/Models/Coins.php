<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coins extends Model
{
    use HasFactory;

    public $timestamps = false;

    protected $fillable = [
        'id',
        'rank',
        'name',
        'symbol',
        'slug',
        'is_active',
        'first_historical_data',
        'last_historical_data',
        'platform'
    ];

}
