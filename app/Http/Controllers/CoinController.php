<?php

namespace App\Http\Controllers;

use App\GetCoins;
use App\Models\Coins;
use Illuminate\Http\Request;

class CoinController extends Controller
{
    public function index()
    {
        $coins = Coins::simplePaginate(25);
        return view('welcome', ['coins' => $coins]);
    }
}
