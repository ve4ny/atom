<?php

namespace App;

use App\Models\Coins;
use Carbon\Carbon;
use DateTime;
use Illuminate\Support\Facades\Http;

class GetCoins
{
    public static function getAll(string $key)
    {
        $coins = Http::withHeaders([
            'X-CMC_PRO_API_KEY' => $key,
        ])->get('https://pro-api.coinmarketcap.com/v1/cryptocurrency/map');

        return $coins['data'];
    }
}
