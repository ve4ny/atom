<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Atom test</title>
    </head>
    <body>
      <table>
          <thead>
          <tr>
              <th>ID</th>
              <th>Name</th>
              <th>Symbol</th>
              <th>Slug</th>
              <th>Is Active</th>
              <th>First Historical Data</th>
              <th>Last Historical Data</th>
              <th>Platform</th>
          </tr>
          </thead>
          <tbody>
          @foreach ($coins as $coin)
              <tr>
                  <td>{{ $coin->id }}</td>
                  <td>{{ $coin->name }}</td>
                  <td>{{ $coin->symbol }}</td>
                  <td>{{ $coin->slug }}</td>
                  <td>
                      @if($coin->is_active)
                          True
                      @else
                          False
                      @endif
                  </td>
                  <td>{{$coin->first_historical_data}}</td>
                  <td>{{$coin->last_historical_data}}</td>
                  <td>{{$coin->platform}}</td>
              </tr>
          @endforeach
          </tbody>
      </table>
      {{ $coins->links() }}
    </body>
</html>
